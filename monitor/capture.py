import cv2
import numpy


def videoCapture():
    cv2.namedWindow("preview")
    # cv2.namedWindow("preview1")
    codec=cv2.VideoWriter_fourcc(*'XVID')
    output=cv2.VideoWriter('/tmp/output.avi',codec,20.0,(640,480))#file to save to
    capture = cv2.VideoCapture(0)
    if capture.isOpened(): # try to get the first frame
        while True:
            rval, frame = capture.read()
            greyscale=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
            # Draw on image
            cv2.line(frame,(0,0),(150,120),(0,0,255))
            cv2.rectangle(frame, (0, 0), (150, 120), (0, 255, 0))
            cv2.circle(frame, (0, 0), 50 , (255, 0, 0))

            points=numpy.array([[6,5],[86,7],[333,66],[303,33]],numpy.int32)
            cv2.polylines(frame,[points],True,(255,255,255),7)
            # cv2.putText(frame,'This is Mutwiso',(3,4), cv2.FONT_HERSHEY_SIMPLEX, 2, 255)

            output.write(frame) #write to output file
            cv2.imshow("preview", frame)
            # cv2.imshow("preview1", greyscale)
            key = cv2.waitKey(20)
            if key == 27: # exit on ESC
              break
    else:
        rval = False
    output.release()
    capture.release()
    # cv2.destroyWindow("preview"
    cv2.destroyAllWindows()

videoCapture()




def imageRead():
        cv2.imread('home/wndessy/Pictures/Awsome\ me/IMG_0062.JPG')