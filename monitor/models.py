# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class File(models.Model):
    """Each pgn file will be stored here """
    Name1 = models.CharField(null=True, max_length=255, unique=True)
    Type = models.CharField(max_length=255, null=True) # Player or opening


class Game(models.Model):
    """ This is a game """
    Black = models.CharField(max_length=255, null=True)
    BlackElo = models.IntegerField(null=True)

    White = models.CharField(max_length=255, null=True)
    WhiteElo = models.IntegerField(null=True)

    Date = models.DateField(null=True)
    Site = models.CharField(max_length=255, null=True)
    Round = models.CharField(max_length=10, null=True)

    Result = models.CharField(max_length=7)
    ECO = models.CharField(max_length=5, null=True)
    PGN = models.TextField(null=False, default='')
    Event = models.CharField(null=True, default='', max_length=255,)
    file = models.ForeignKey(File, related_name='games', on_delete=models.CASCADE)


class Position(models.Model):
    """The different positions in the game """
    game = models.ForeignKey(Game, related_name='positions', on_delete=models.CASCADE)
    played_move = models.CharField(max_length=7)
    position = models.CharField(max_length=92)